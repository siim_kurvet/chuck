import { observable, action } from "mobx";
import {ICategoryMap, IFact} from "../types";
import { fetchFromApi } from "../util/ApiServices";

export default class ChuckStore {
    
    @observable private categories: ICategoryMap = {};
    @observable private selectedCategory: string = "";

    constructor() {
        this.createCategories();
    }

    public getGategories(): ICategoryMap {
        return this.categories;
    }

    public getSelectedCategory(): string {
        return this.selectedCategory;
    }

    public getFacts(): IFact[] {
        return this.categories[this.selectedCategory];
    }

    @action
    public setSelectedCategory = (selectedCategory: string): void => {
        this.selectedCategory = selectedCategory;
    }

    @action 
    public toggleFactStatus = (factId: string): void => {
        const fact: IFact | undefined = this.getFact(factId, this.selectedCategory);
        if(fact !== undefined) {
            fact.selected = !fact.selected;
        }
    }

    private getFact(factId: string, category: string): IFact | undefined {
        return this.categories[this.selectedCategory].filter((fact: IFact) => {
            return fact.id === factId;
        }).pop();
    }

    private createCategories(): void {
        const url: string = "https://api.chucknorris.io/jokes/categories";
        fetchFromApi(url).then((response: string[]) => {
            response.forEach(action((categoryName: string) => {
                this.categories[categoryName] = [];
                this.fetchThreeUniqueFacts(categoryName);
            }))
        }).catch((err: any) => console.log(err))
    }

    private async fetchThreeUniqueFacts(category: string) {
        const url: string = "https://api.chucknorris.io/jokes/random?category=" + category;
        // search 10 times for 3 unique values, to avoid infinite loop in case some category has less than 3 facts.
        for(let i = 0; i < 10; i++) {
            const fact: IFact = await fetchFromApi(url);
            if(this.isFactUnique(category, fact.id)) {
                this.addFact(category, fact);
            }
            if(this.categories[category].length >= 3) {
                break;
            }
        }
    }

    @action
    private addFact(category: string, fact: IFact) {
        this.categories[category].push({
            id: fact.id,
            selected: false,
            value: fact.value
        });
    }

    private isFactUnique(category:string, factId: string): boolean {
        return this.categories[category].filter((fact: IFact) => {
            return fact.id === factId;
        }).length === 0;
    }
}