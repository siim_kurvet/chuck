import {configure} from 'mobx';
import ChuckStore from './ChuckStore'

configure({enforceActions: 'observed'});

class RootStore {
    public chuckStore: ChuckStore;

    constructor() {
        this.chuckStore = new ChuckStore();
    }
}

export default new RootStore()