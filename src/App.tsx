import React from 'react';
import './App.scss';
import { observer, Provider } from 'mobx-react';
import rootStore from "./stores/RootStore";
import Content from './components/Content';

const App: React.FC = observer(() => {

  return (
    <Provider {...rootStore}>
      <div className="App">
        <header className="App-header">
          <Content />
        </header>
      </div>
    </Provider>
  );
})

export default App;
