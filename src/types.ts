export interface ICategoryMap {
    [category: string]: IFact[]
}

export interface IFactJson {
    icon_url?: string,
    id: string,
    url?: string,
    value: string
}

export interface IFact extends IFactJson {
    selected: boolean
}