function parseJSON(response: Response) {
    return response.json();
}

function handleErrors(response: Response) {
    if (!response.ok) {
         throw Error(response.status.toString());
    }
    return response;
}

export function fetchFromApi(url: string): Promise<any> {
    return fetch(encodeURI(url), {credentials: 'include' })
        .then(handleErrors)
        .then(parseJSON)
        .catch(err => console.log("ERROR: " + err.message));
}