import { MobXProviderContext } from "mobx-react";
import * as React from "react";

export function useStores() {
    return React.useContext(MobXProviderContext)
}