import { observer } from "mobx-react";
import * as React from "react";
import { useStores } from "../util/StoreServices";
import { IFact } from "../types";

interface ICategoryComponent {
    name: string
}

const Category: React.FC<ICategoryComponent> = observer((props) => {

    const { chuckStore } = useStores()

    const numberOfFavorites = (): number => {
        return chuckStore.getGategories()[props.name].filter((fact: IFact) => {
            return fact.selected;
        }).length
    }

    return (
        <div className={"category" + (chuckStore.getSelectedCategory() === props.name ? " selected" : "")} 
            onClick={() => chuckStore.setSelectedCategory(props.name)}>
            <span className="name">{props.name.toUpperCase()}</span>
            {numberOfFavorites() > 0 && 
                <span className="counter">{numberOfFavorites()}</span>
            }
        </div>
    );
});

export default Category;