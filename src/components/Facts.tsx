import { observer } from "mobx-react";
import * as React from "react";
import { IFact } from "../types";
import { useStores } from "../util/StoreServices";
import { FaRegStar, FaStar } from 'react-icons/fa';

const Fact: React.FC<{fact: IFact}> = observer((props) => {

    const { chuckStore } = useStores()

    return (
        <div className="fact"  onClick={() => chuckStore.toggleFactStatus(props.fact.id)}>
            {props.fact.selected ?
                <FaStar className="favourite"/>
                :
                <FaRegStar className="favourite"/>
            }
            {props.fact.value}
        </div>
    );
});

const Facts: React.FC<{facts: IFact[]}> = observer((props) => {

    if(props.facts !== undefined) {
        return (
            <>
                {props.facts.map((fact: IFact, index: number) => {
                    return (<Fact key={index} fact={fact}/>)
                })}
            </>
        );
    } else {
        return <p className="placeholder">Select a category...</p>
    }
    
});

export default Facts;