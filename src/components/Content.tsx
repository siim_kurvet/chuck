import React, { useState } from 'react';
import chuck from '../chuck_fist.png'
import { observer } from 'mobx-react';
import Category from "./Category"
import { Container, Row, Col} from 'reactstrap';
import Facts from './Facts';
import { useStores } from '../util/StoreServices';
import { FaBars } from 'react-icons/fa';

const Content: React.FC = observer(() => {

    const [hideMobileMenu, toggleMobileMenu] = useState(true);

    const { chuckStore } = useStores()

    return (
        <Container>
            <Row>
                <div className="mobile-menu" onClick={() => toggleMobileMenu(!hideMobileMenu)}>
                    <span>CATEGORIES</span>
                    <FaBars className="bars"/>
                </div>
                <Col sm="4" md="3" lg="2" className={"menu" + (hideMobileMenu ? " hide" : "")}>
                    {
                    Object.keys(chuckStore.getGategories()).map((key: string, index: number) => {
                        return (<Category key={index} name={key}/>);
                    })
                    }
                </Col>
                <Col sm="8" md="9" lg="10" className="content">
                    <div>
                    {chuckStore.getSelectedCategory() === "" &&
                        <img src={chuck} className="App-logo" alt="logo" />
                    }
                    <Facts facts={chuckStore.getFacts()}/>
                    </div>
                </Col>
            </Row>
        </Container>
    );
  })
  
  export default Content;